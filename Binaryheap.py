class BinHeap:
    def __init__(self):
        self.heapList = [0]
        self.currentSize = 0

    def move_up(self,i):
        while i//2 >0:
            if self.heapList[i] <self.heapList[i//2]:
                tmp = self.heaplist[i//2]
                self.heapList[i//2] = self.heapList[i]
                self.heapList[i]=tmp
            i=i//2

    def insert(self,element):
        self.heapList.append(element)
        self.currentSize = self.currentSize +1
        self.move_up(self.currentSize)

    def move_down(self,i):
        while (i*2)<=self.currentSize:
            minchildindex = self.min_child(i)
            if self.heapList[i] >self.heapList[i//2]:
                tmp = self.heapList[i]
                self.heapList[i] = self.heapList[minchildindex]
                self.heapList[minchildindex]=tmp
            i=minchildindex

    def min_child(self,i):
        if i * 2 + 1 > self.currentSize:
            return i*2
        else:
            if self.heapList[i*2]>self.heapList[i*2+1]:
                return i * 2
            else:
                return i*2+1

    def del_min(self):
        return_value = self.heapList[1]
        self.heapList[1] = self.heapList[self.currentSize]
        self.currentSize = self.currentSize -1
        self.heapList.pop()
        self.move_down(1)
        return return_value

    def build_heap(self,alist):
        i=len(alist)//2
        self.currentSize = len(alist)
        self.heapList= [0] + alist[:]
        while i>0:
            self.move_down(i)
            i-=1
